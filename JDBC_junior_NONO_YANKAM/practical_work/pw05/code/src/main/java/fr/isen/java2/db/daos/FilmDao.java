/**
 * @author junior NONO YANKAM
 *
 */
package fr.isen.java2.db.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.isen.java2.db.entities.Film;
import fr.isen.java2.db.entities.Genre;

public class FilmDao {

	public List<Film> listFilms() throws SQLException {
		List<Film> list=new ArrayList<>() ;
		String sql= "SELECT * FROM film JOIN genre ON film.genre_id = genre.idgenre";
		try(Connection connection =  DataSourceFactory.getConnection()){
			try(Statement statement= connection.createStatement()){
				try(ResultSet results= statement.executeQuery(sql)){
					while(results.next()) {
						Film film= new Film(
								results.getInt("idfilm"),
								results.getString("title"),
								results.getDate("release_date").toLocalDate(),
								new Genre(),
								results.getInt("duration"),
								results.getString("director"),
								results.getString("summary")
								);
								
						list.add(film);
					}
				}
			}
		}
		//System.out.println("la liste"+list);
		return list;
	}

	public List<Film> listFilmsByGenre(String genreName) {
		List<Film> list = new ArrayList<>();
		
		try(Connection connection= DataSourceFactory.getConnection()){
			String sql = "SELECT * FROM film, genre WHERE film.genre_id = genre.idgenre AND genre.name = ?";
			try(PreparedStatement statement = connection.prepareStatement(sql)){
				statement.setString(1, genreName);
				try(ResultSet results= statement.executeQuery()){
					while(results.next()) {
						Film film= new Film(
								results.getInt("idfilm"),
								results.getString("title"),
								results.getDate("release_date").toLocalDate(),
								new Genre(),
								results.getInt("duration"),
								results.getString("director"),
								results.getString("summary")
								);
								
						list.add(film);
					}
				}
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public void addFilm(Film film) {
		try(Connection connection= DataSourceFactory.getConnection()){
			String sql = "INSERT INTO film(idfilm,title,release_date,genre_id,duration,director,summary) values(?,?,?,?,?,?,?)";
			try(PreparedStatement statement = connection.prepareStatement(sql)){
				statement.setInt(1, film.getId());
				statement.setString(2, film.getTitle());
				statement.setDate(3, java.sql.Date.valueOf(film.getReleaseDate()));
				statement.setInt(4, film.getGenre().getId());
				statement.setInt(5, film.getDuration());
				statement.setString(6, film.getDirector());
				statement.setString(7, film.getSummary());
				
				int nbRows= statement.executeUpdate();
				System.out.println("nb de lignes :"+nbRows);
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
}
