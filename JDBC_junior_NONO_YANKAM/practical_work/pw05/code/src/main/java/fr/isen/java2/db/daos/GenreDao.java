
/**
 * @author junior NONO YANKAM
 *
 */

package fr.isen.java2.db.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.isen.java2.db.entities.Film;
import fr.isen.java2.db.entities.Genre;

public class GenreDao {

	public List<Genre> listGenres() throws SQLException {
		List<Genre> list=new ArrayList<>() ;
		String sql= "SELECT * FROM genre";
		try(Connection connection =  DataSourceFactory.getConnection()){
			try(Statement statement= connection.createStatement()){
				try(ResultSet results= statement.executeQuery(sql)){
					while(results.next()) {
						Genre genre= new Genre(
								results.getInt("idgenre"),
								results.getString("name")
								);
								
						list.add(genre);
					}
				}
			}
		}
		//System.out.println("la liste"+list);
		return list;
	}

	public Genre getGenre(String name) {
		Genre genre = null;
		
		try(Connection connection= DataSourceFactory.getConnection()){
			String sql = "SELECT * FROM genre WHERE name = ?";
			try(PreparedStatement statement = connection.prepareStatement(sql)){
				statement.setString(1, name);
				try(ResultSet results= statement.executeQuery()){
					while(results.next()) {
						 genre= new Genre(
								results.getInt("idgenre"),
								results.getString("name")
								);
								
					}
				}
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return genre;
	}

	public void addGenre(String name) {
		try(Connection connection= DataSourceFactory.getConnection()){
			String sql = "INSERT INTO genre(name) values(?)";
			try(PreparedStatement statement = connection.prepareStatement(sql)){
				statement.setString(1, name);
				
				int nbRows= statement.executeUpdate();
				System.out.println("nb de lignes :"+nbRows);
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
}
